from keras.models import model_from_json
import pickle
import flask
from flask import request,jsonify
import numpy as np

np.random.seed(1234)
from pymongo import MongoClient
client = MongoClient()
client = MongoClient('localhost', 27017)
db = client['user_requests']
user_requests = db.user_requests
# initialize our Flask application and the Keras model
app = flask.Flask(__name__)
model = None


@app.route("/users", methods=["POST"])
def predict():
    response = {"success": False}
    final_data = request.json["input"]
    session_id=final_data["session_id"]
    time_stamp=final_data["timestamp"]
    user_id = final_data["user_id"]
    utterances=final_data["utterances"][0]["utterance"]


    user_requests.insert(final_data)
    print("inserted values in user_requests database")

    response["success"] = True
    # return the data dictionary as a JSON response
    return flask.jsonify(response)




if __name__ == "__main__":


    app.run(debug=True)