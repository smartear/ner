import random
#templated responses
INCOMING_MESSAGE_READ={"confirm":["_SENDER_ sent you a message. Do you wnat me to open it for you?","Got a message from _SENDER_. Do you want me to read it?","Incoming message from _RECEPIENT_.Do you want me to read it?"],
                       "execute":["This is what _SENDER_ said._MESSAGE_.","Message from _SENDER_ said _MESSAGE_."]}

MAKE_CALL={"find":["Sorry, cannot find that user or group name. Can you please spell it?"],
           "confirm":["I am about to make a call to _RECEPIENT_.Do you confirm?","Okay, calling them now.Do yoou confirm?","Do you want to call _RECEPIENT_?"],
           "execute":["Alright, making your call.","Alright, calling the now"],
           "request":["Who are you trying to call?You should specify a recepient"],
           "select":["I have _NUMBER_ contacts for you._RECEPIENT_LIST_.Who do you want to call?","Here are _NUMBER_ contacts for you._RECEPIENT_LIST_.Who do you want me to call?"],
           "others":["Sorry, I am not able to find _RECEPIENT_ in your contact_list","Sorry, there is no _RECEPIENT_ in your contact list."]}

SEND_MESSAGE={"find":["Sorry, cannot find that user or group name. Can you please spell it?"],
              "confirm":["You are about to send a message to _RECEPIENT_.Ready to send it?","Are you sure you want to send a message to _RECEPIENT_.","Sending your message to _RECEPIENT_.Do you confirm?","Do you want your message to _RECEPIENT_ to be sent?"],
              "execute":['Okay. I am sending your message','Alright, sendiing your message','I am sending your message.'],
              "request":['Who is the recepient of this message','Who would you like to send a message to'],
              "select":["I have _NUMBER_ contacts for you._RECEPIENT_LIST_.Please select one of those options","Here are _NUMBER_ contacts for you._RECEPIENT_LIST_.Who do you want me to message?"],
              "others":["Sorry, I am not able to find _RECEPIENT_ in your contact_list","I could'nt find any of these contacts.","It does not seem you have _RECEPIENT_ in your contacts"]}

#machine_actions
machine_actions={"new":0,"find":1,"confirm":2,"execute":3,"request":4,"select":5,"others":6}

def generate_MAKE_CALL_responses(actual_recepient,recepient_list,request_flag,users_data):
    data={}
    user_response=''
    machine_action=''

    if request_flag==1:
        machine_action = "request"
        user_response = random.choice(MAKE_CALL[machine_action])
        data = {'data': {'encoding': 'json',
        'payload': '{"protocol_request": false, "message_request": false, "contact_request": true}',
        'schema': 'message_request_data'}}




    elif len(recepient_list)==1:
        machine_action="confirm"
        user_response=random.choice(MAKE_CALL[machine_action])
        user_response = user_response.replace("_RECEPIENT_", recepient_list[0])
        data: {'data': {'encoding': 'json',
                 'payload': '{"protocol_value": "mobile", "contact_value": [[{"value": "Richard Smith", "type": "name"}, {"value": "mobile", "type": "protocol"}, {"value": "6479745453", "type": "address"}]], "message_value": "please give me a call when you\'re free"}',
                 'schema': 'message_execute_data'}}


    elif len(recepient_list)>1:
        machine_action="select"
        user_response = random.choice(MAKE_CALL[machine_action])
        user_response = user_response.replace("_RECEPIENT_", ' '.join(recepient_list))
        user_response = user_response.replace("_NUMBER_",len(recepient_list))

        data={'data': {'encoding': 'json',
                 'payload': '{"message_options": [], "protocol_options": [], "contact_option": [[{"value": "Kyle", "type": " name"}], [{"value": "Charlie", "type": " name"}]]}',
                 'schema': 'message_select_data'}}

    elif len(recepient_list)==0:
        machine_action="others"
        user_response = random.choice(MAKE_CALL[machine_action])
        user_response = user_response.replace("_RECEPIENT_", actual_recepient)
        data={'data': {}}

    final_data = {"data":data,"user_id": users_data['user_id'], "timestamp": users_data['timestamp'], "user_intent": 'MAKE_CALL',
              "user_response": user_response, 'session_id': users_data['session_id'],'machine_action': machine_action}

    return final_data






