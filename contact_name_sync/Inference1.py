
from keras.models import model_from_json

import random

import pandas as pd

from nltk import FreqDist
import numpy as np
import flask
from flask import request,jsonify
import nltk
import numpy as np
np.random.seed(1234)

# initialize our Flask application and the Keras model
app = flask.Flask(__name__)
model = None
from keras_contrib.utils import save_load_utils
@app.route("/predict1", methods=["POST"])
def predict():


    input1=request.json["input"]
    final_data={"predictions":{},"success": False}

    second_tags = {}
    comm = {}
    options = [
        "please",
        "hurry and",
        "go ahead and",
        "do me a favor and"
    ]

    commands = [
        "tell _RRN_ that_OPT _MMM_.",
        "I need to tell _RRN_ that_OPT."
        "can you tell _RRN_ that_OPT _MMM_?",
        "can you please tell _RRN_ that_OPT _MMM_?",
        "send a message to _RRN_ to say that_OPT _MMM_.",
        "ping _RRN_ to tell _RRG_ that_OPT _MMM_.",
        "shoot a message to _RRN_ that_OPT _MMM_.",
        "send _RRN_ that_OPT _MMM_.",
        "let _RRN_ know that_OPT _MMM_.",
        "could you please send a message to _RRN_ that_OPT _MMM_?",
        "could you send a message to _RRN_ that_OPT _MMM_?",
        "can you please send a message to _RRN_ that_OPT _MMM_?",
        "can you send a message to _RRN_ that_OPT _MMM_?",
        "I need _RRN_ to know that_OPT _MMM_.",
        "Would you mind telling _RRN_ that_OPT _MMM_.",
        "Go ahead and tell _RRN_ that_OPT _MMM_.",
        "I need to tell _RRN_ that_OPT _MMM_.",
        "I need _RRN_ to know that_OPT _MMM_.",
        "_RRN_ needs to know that_OPT _MMM_.",
        "it would be great if _RRN_ knew that_OPT _MMM_.",
        "notify _RRN_ that_OPT _MMM_.",
        "inform _RRN_ that_OPT _MMM_.",
        "contact _RRN_ to tell _RRG_ that_OPT _MMM_.",
        "message _RRN_ that_OPT _MMM_."
    ]

    messages = [
        "I will be late",
        "I won't be coming tonight",
        "I am stuck in traffic",
        "dinner is now at 9:00pm",
        "I cannot be there for the meeting tomorrow",
        "_RRO_ won't be coming for dinner",
        "I need _RRO_ to pick me up from work soon",
        "my kids are sick and I need to work from home today",
        "I will call him back later",
        "we need need sheet in room _NUM_",
        "table _NUM_ is asking for the desserts menu",
        "table _NUM_ is getting impatient",
        "we need a cleanup aisle _NUM_",
        "the customer is very unhappy",
        "I need security on floor _NUM_",
        "I need help in section _NUM_",
        "room _NUM_ hasn't checked out yet",
        "I need fresh towels for room _NUM_, and fast",
        "there is an emergency on floor _NUM_",
        "_RRO_ might need some extra help cleaining room _NUM_",
        "I have an urgent message",
        "this is not very funny",
        "it's almost the end of the night shift",
        "I am so happy that it's almost the weekend",
        "the people in room _NUM_ seem to have forgotten a sweater",
        "the elevator is stuck again",
        "there is a situation on floor _NUM_",
        "the order for room _NUM_ isn't ready yet",
        "the customer in room _NUM_ is asking for more shampoo",
        "I am running out of shampoo on floor _NUM_",
        "there is a leak on floor _NUM_",
        "I need immediate backup on floor _NUM_",
        "it would be great if I could have some backup soon.",
        "I will need help to lift the bed in room _NUM_",
        "I need more cleaning products in room _NUM_",
        "the air-conditioning in room _NUM_ is out of service",
        "we will need an electrician on floor _NUM_",
        "the lock in room _NUM_ seems damaged",
        "we are out of tea bags on floor _NUM_",
        "_RRO_ is asking for help in room _NUM_",
        "_RRO_ is late and won't be there for the beginning of the next shift",
        "I can't believe how messy people in room _NUM_ are",
        "the lightbulb in room _NUM_ needs to be changed",
        "there is a power outage on floor _NUM_",
        "we need a first aid kit on floor _NUM_",
        "we need a spare key for the closet on floor _NUM_",
        "the carpet in room _NUM_ needs to be cleaned today",
        "I need new pillows for room _NUM_"
    ]

    cols = ['names', 'freq', 'cumul_freq', "pos"]

    DF_names_F = pd.read_csv("census-dist-female-first.txt", names=cols, sep="\s+")
    # DF_names_F=['Richa','Alicia','Asha']
    # DF_names_M=['Amar','Adarsh','Kian']
    DF_names_M = pd.read_csv("census-dist-male-first.txt", names=cols, sep='\s+')

    names_F = DF_names_F['names'].tolist()
    # names_F=['Richa','Alicia','Asha']
    # names_M=['Amar','Adarsh','Kian']
    names_M = DF_names_M['names'].tolist()

    def tag_string(sentence, tag):
        # sentence is a series of words (part of a sentence)
        series = sentence.split(" ")
        res = ""
        if tag == "O":
            for w in series:
                res += w + "\O" + " "
            return res
        res += series[0] + "\B" + tag + " "
        for w in series[1:]:
            res += w + "\I" + tag + " "
        return res

    max = 50
    data = []
    n = 0
    frac_male = 0.5

    while n < max:
        option = ""
        if random.random() < 0.01:
            option = random.choice(options) + " "
        command = random.choice(commands)
        if random.random() < frac_male:
            recipient = random.choice(names_M).capitalize()
            gender = "him"
        else:
            recipient = random.choice(names_F).capitalize()
            gender = "her"
        tag_recipient = recipient + "\R"
        message = random.choice(messages)

        tag_message = tag_string(message, "M")
        command = option + command

        tag_command = command
        command = command.replace("_RRN_", recipient)
        command = command.replace("_RRG_", gender)
        command = command.replace("_MMM_", message)
        tag_command = tag_command.replace("_RRN_", tag_recipient)
        tag_command = tag_command.replace("_RRG_", gender)
        tag_command = tag_command.replace("_MMM_", tag_message)
        if random.random() < 0.3:
            command = command.replace("that_OPT ", "")
            tag_command = tag_command.replace("that_OPT ", "")
        else:
            command = command.replace("that_OPT ", "that ")
            tag_command = tag_command.replace("that_OPT ", "that ")
        if "_RRO_" in command:
            if random.random() < 0.5:
                temp_name = random.choice(names_F).capitalize()
                command = command.replace("_RRO_", temp_name)
                tag_command = tag_command.replace("_RRO_", random.choice(names_F).capitalize())
            else:
                temp_name = random.choice(names_F).capitalize()
                command = command.replace("_RRO_", temp_name)
                tag_command = tag_command.replace("_RRO_", temp_name)
        if "_NUM_" in command:
            command = command.replace("_NUM_", str(random.randint(0, 50)))
            tag_command = tag_command.replace("_NUM_", str(random.randint(0, 50)))
        command = command[0].capitalize() + command[1:]
        tag_command = tag_command[0].capitalize() + tag_command[1:]
        comm[command] = tag_command

        n += 1

    first_tags = {}

    for values in comm.keys():

        l = comm[values].split(" ")
        first_tags[values] = [0] * 30
        for values_l in l:

            if "\\R" in values_l:
                first_tags[values][l.index(values_l)] = 1

            if "\\BM" in values_l or "\\IM" in values_l:
                first_tags[values][l.index(values_l)] = 2
    second_tags = []
    for values in comm.keys():

        l = comm[values].split(" ")

        if "tell" in l or "knew" in l or "know" in l or "telling" in l or "Tell" in l:
            second_tags.append(0)
        else:
            second_tags.append(1)

    tags = second_tags
    data = list(comm.keys())
    train = data[:45]
    test = data[45:50]
    train2_tags = tags[:45]
    test2_tags = tags[45:50]
    tags1 = list(first_tags.values())
    train1_y = tags1[:45]
    test1_y = tags1[45:]

    from numpy import array
    from keras.utils import to_categorical
    y = [to_categorical(i, num_classes=0) for i in train1_y]
    y2 = [to_categorical(i, num_classes=0) for i in test1_y]
    y2 = array(y2)
    y = array(y)

    X_train = [nltk.word_tokenize(x) for x in train]
    X_test = [nltk.word_tokenize(x) for x in test]

    x_distr = FreqDist(np.concatenate(X_train + X_test))
    x_vocab = x_distr.most_common(min(len(x_distr), 10000))

    x_idx2word = [word[0].lower() for word in x_vocab]

    x_idx2word.insert(0, '<PADDING>')
    x_idx2word.append('<UNK>')

    x_word2idx = {word: idx for idx, word in enumerate(x_idx2word)}

    x_train_seq = np.zeros((len(X_train), 30), dtype=np.int32)
    for i, da in enumerate(X_train):
        for j, token in enumerate(da):
            # truncate long Titles
            if j >= 30:
                break
            token=token.lower()
            # represent each token with the corresponding index
            if token in x_word2idx:
                x_train_seq[i][j] = x_word2idx[token]
            else:
                x_train_seq[i][j] = x_word2idx['<UNK>']

    x_test_seq = np.zeros((len(X_test), 30),
                          dtype=np.int32)  # padding implicitly present, as the index of the padding token is 0

    # form embeddings for samples testing data
    for i, da in enumerate(X_test):
        for j, token in enumerate(da):
            # truncate long Titles
            if j >= 30:
                break
            token=token.lower()
            # represent each token with the corresponding index
            if token in x_word2idx:
                x_test_seq[i][j] = x_word2idx[token]
            else:
                x_test_seq[i][j] = x_word2idx['<UNK>']

    def get_encoding(test):

        test = test.split()[:-1]

        value = np.zeros((1, 30), dtype=np.int32)


        for j, token in enumerate(test):
            # truncate long Titles

            if j >= 30:
                break
            token = token.lower()
            # represent each token with the corresponding index
            if token in x_word2idx:
                value[0][j] = x_word2idx[token]
            else:
                value[0][j] = x_word2idx['<UNK>']
        return value

    x_test_seq=get_encoding(input1)

    predicted = loaded_model.predict(x_test_seq)

    pred=[np.argmax(y) for y in predicted[0]]
    print(pred)

    i=0
    message=[]
    recepient=''

    input1=str(input1).split(" ")

    for i in range(len(pred)):
        if pred[i]==1:
            recepient=input1[i]
        if pred[i]==2:
            if i>=len(input1):
                message.append(" ")
            else:

                message.append(input1[i])
        i=i+1


    final_data["predictions"]={"recepient": recepient, "message": ' '.join(message)}
    final_data["success"] = True

    # return the data dictionary as a JSON response
    return flask.jsonify(final_data)

if __name__ == "__main__":
    print(("* Loading Keras model and Flask starting server..."
        "please wait until server has fully started"))
    json_file = open('model.json', 'r')
    loaded_model_json = json_file.read()
    json_file.close()
    loaded_model = model_from_json(loaded_model_json)
    print("Loaded model from disk")
    # load weights into new model
    loaded_model.load_weights("model.h5")
    app.run()