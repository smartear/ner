from flask import Flask
from flask import jsonify
from flask import request
from flask_pymongo import PyMongo

app = Flask(__name__)

app.config['MONGO_DBNAME'] = 'test-database'
app.config['MONGO_URI'] = 'mongodb://localhost:27017/test-database'

mongo = PyMongo(app)

@app.route('/star', methods=['GET'])
def get_all_stars():
  posts = mongo.db.posts
  
  output = []
  
  for s in posts.find():
    
    output.append({'author' : s['author'], 'text' : s['text']})
  print(output)
  return jsonify({'result' : output})
  
@app.route('/star/', methods=['GET'])
def get_one_star():
  name=request.args.get('author')
  author = mongo.db.post2.find_one({"author": name})
  books = mongo.db.post2.find({"tags": {"$in": author['tags']}})
  output=[]

  if books:
    for v in books:
       
     output.append( v['author'])
  else:
    output = "No such name"
  return jsonify({'result' : output})
  
if __name__ == '__main__':
    app.run(debug=True)