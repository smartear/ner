This piece of code is used to convert audio files to textual format:
1. We will make request to Google Cloud ASR using API calls
2. The request should be of this form:

{
  "config": {
      "encoding":"FLAC",
      "languageCode": "en-US"
  },
  "audio": {
      "content":BINARY
  }
}
Hence, we will form our requests accordingly, bow we need the binary:

3. In Windows, you can do this by execting the certutil command
certutil -encode audio_file_name +outputFileName

4.After you do this, lets create the request call
It will be a post request 'https://speech.googleapis.com/v1/speech:recognize?key=AIzaSyAXQDexAHJwLb_4iaEAVkuXjYoMqqZVcuE' where key is the access key.
 

