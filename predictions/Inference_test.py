from keras.models import model_from_json
import pickle
import flask
from flask import request,jsonify
import numpy as np
import pymongo as pymongo
import sys
sys.path.append("..")
import generate_response
#import contact_name_matching
#import generate_phonemes
from pymongo import MongoClient
np.random.seed(1234)

client = MongoClient()
client = MongoClient('localhost', 27017)
db = client['user_requests']
user_requests = db.user_requests
timeline=db.timeline

# initialize our Flask application and the Keras model
app = flask.Flask(__name__)
model = None

from g2p_en import g2p

y = []
def grapheme_to_phoneme(test_list):
    result=[]
    for values in test_list:
        inter = g2p(values)
        inter=give_result(inter)
        result.append(inter)
    return result

def give_result(text):
    result = []
    final_result=[]
    for values in text:
        S = ''
        for d in values:

            if d.isdigit() == False and d !=' ':
                S = S + d

        result.append(S)
    for values in result:
        if values is not '':
            final_result.append(str(values))
    return final_result
import numpy as np
from sklearn.metrics.pairwise import cosine_similarity as cos_sim

np.random.seed(1234)
from pymongo import MongoClient

client = MongoClient()
client = MongoClient('localhost', 27017)
db = client['user_requests']
contact_groups = db.contact_groups


def load_contacts(sender):

    sender=db.contacts_users.find_one({"uid":sender})
    sender_name=sender['username']
    print(sender_name)
    users = db.contact_groups.find({"list_users": {"$in": [sender_name]}})

    contact_list = []

    for values in users:

        for v in values['list_users']:
            if v != sender_name and v not in contact_list:
                contact_list.append(v)

    users = db.contacts_users.find({"username": {"$in": contact_list}})

    full_name_phoneme = []
    contact_list = []
    first_name_phoneme = []
    last_name_phoneme = []
    nick_name_phoneme = []

    for values in users:

        contact_list.append(values['username'])
        if len(values['names_phonemes']) > 3:
            first_name_phoneme.append(values['names_phonemes'][0])
            last_name_phoneme.append(values['names_phonemes'][1])
            full_name_phoneme.append(values['names_phonemes'][2])
            nick_name_phoneme.append(values['names_phonemes'][3])

    return first_name_phoneme, last_name_phoneme, full_name_phoneme, nick_name_phoneme, contact_list


def compare_phonemes(P1, P2):
    score = 0
    for p1, p2 in zip(P1, P2):
        score += cos_sim(y.index(p1), y.index(p2))

    return score


def find_best_sub_match(phoneme1, phoneme2):

    phoneme1=phoneme1[0]
    l = len(phoneme1)
    L = len(phoneme2)
    highest_score = 0
    for i in range(l + L - 1):
        P1 = phoneme1[max(0, l - 1 - i):l + L - i - 1]
        P2 = phoneme2[max(0, i - l + 1):i + 1]
        highest_score = max(highest_score, compare_phonemes(P1, P2))
    return highest_score


def match_contacts(user_says, user_says_phoneme, sender):

    with open('phone_features.txt', 'r') as f:
        for line in f:
            line = line.strip('\n').split(',')
            y.append(line[0])

    first_name_phoneme, last_name_phoneme, full_name_phoneme, nick_name_phoneme, contact_list = load_contacts(
        sender)

    for matching_name in [full_name_phoneme, first_name_phoneme, last_name_phoneme, nick_name_phoneme]:
        for i in range(len(matching_name)):
            u_score = []
            c_score = []
            foo = []
            # print(matching_name[i],user_says_phoneme)
            match_score = find_best_sub_match(user_says_phoneme, matching_name[i])

            match_score_norm_speech = np.round(match_score / len(user_says), 4)
            match_score_norm_contact = np.round(match_score / len(matching_name[i]), 4)

            # content score, user score
            u_score.append(match_score_norm_speech)
            c_score.append(match_score_norm_contact)

        top_matches_by_u_score = list(zip(contact_list,
                                          u_score))
        top_matches_by_c_score = list(zip(contact_list,
                                          c_score))

        top_matches_by_u_score.sort(key=lambda x: -x[1])
        top_matches_by_c_score.sort(key=lambda x: -x[1])

        # print("matching speech rate")
        foo1 = [x for x in top_matches_by_u_score[:10] if x[1] >= 0.5]
        for x in top_matches_by_u_score[:10]:
            if x[1] >= 0.7:
                foo1.append(x)

        # print("matching contact name rate")
        foo2 = [x for x in top_matches_by_c_score[:10] if x[1] >= 0.5]

        if foo1 and foo2:
            for x in foo1:
                if x[0] in list(zip(*foo2))[0]:
                    i = list(zip(*foo2))[0].index(x[0])
                    foo.append((x[0], x[1], foo2[i][1]))

        foo.sort(key=lambda x: -x[2])
        foo.sort(key=lambda x: -x[1])
        i = i + 1
        result = foo

        persons = []
        if len(result) == 1:
            persons.append(result[0][0])
        else:
            for i in range(1, len(result)):

                if result[i - 1][1] - result[i][1] < 0.1 or result[i - 1][2] - result[i][2] < 0.1:
                    persons.append(result[i - 1])
                i = i + 1
        final_people=[]
        for values in persons:
            final_people.append(values[0])
        return final_people

@app.route("/v1/predict", methods=["POST"])
def predict():
    response = {"success": False}


    input = request.json["input"]

    input1 = input['utterances'][0]['utterance']


    final_data = {"predictions": {}, "success": False}



    x_vocab = pickle.load(open("x_vocab.pickle", "rb"))
    x_test_seq = pickle.load(open("x_test_seq.pickle", "rb"))
    x_train_seq = pickle.load(open("x_train_seq.pickle", "rb"))
    y = pickle.load(open("y.pickle", "rb"))
    y2 = pickle.load(open("y2.pickle", "rb"))
    x_word2idx = pickle.load(open("x_word2idx.pickle", "rb"))

    def get_encoding(test):

        test = test.split()[:-1]

        value = np.zeros((1, 30), dtype=np.int32)

        for j, token in enumerate(test):
            # truncate long Titles

            if j >= 30:
                break
            token = token.lower()
            # represent each token with the corresponding index
            if token in x_word2idx:
                value[0][j] = x_word2idx[token]
            else:
                value[0][j] = x_word2idx['<UNK>']
        return value

    x_test_seq = get_encoding(input1)

    predicted = loaded_model.predict(x_test_seq)

    pred = [np.argmax(y) for y in predicted[0]]


    i = 0
    message = []
    recepient = ''

    input1 = str(input1).split(" ")

    for i in range(len(pred)):
        if pred[i] == 1:
            recepient = input1[i]
        if pred[i] == 2:
            if i >= len(input1):
                message.append(" ")
            else:

                message.append(input1[i])
        i = i + 1

    recepient_list=match_contacts(recepient,grapheme_to_phoneme([recepient]),input['user_id'])


    final_data = request.json["input"]
    session_id=final_data["session_id"]
    time_stamp=final_data["timestamp"]
    user_id = final_data["user_id"]
    utterances=final_data["utterances"][0]["utterance"]

    user_requests.insert(final_data)
    intent='SEND_MESSAGE'

    users_data={"session_id":session_id,"time_stamp":time_stamp,"user_id":user_id}

    print("inserted values in user_requests database for logs")
    data={"session_id":session_id,"time_stamp":time_stamp,"user_id":user_id,"utterances":utterances}

    result_timeline = db.timeline.find_one({"user_id": user_id, "session_id": session_id}, sort=[('_id', pymongo.DESCENDING)])

    if result_timeline==None:

        print("Its a new response")
        time1 = [{"session_id": session_id, "time_stamp": time_stamp, "user_id": user_id, "message": utterances, "machine_action": "new","intent":intent}]
        result = timeline.insert(time1)
        if recepient_list == None:
            machine_action = "request"
            time1 = [{"session_id": session_id, "time_stamp": time_stamp, "user_id": user_id, "message": utterances,
                      "machine_action": "request", "intent": intent}]
            result = timeline.insert(time1)
        elif len(recepient_list)==1:
            machine_action = "confirm"
            time1 = [
                {"session_id": session_id, "time_stamp": time_stamp, "user_id": user_id, "message": utterances,
                 "machine_action": "confirm", "intent": intent}]
            result = timeline.insert(time1)
        elif len(recepient_list) > 0:
            machine_action = "select"
            time1 = [{"session_id": session_id, "time_stamp": time_stamp, "user_id": user_id, "message": utterances,
                      "machine_action": "select", "intent": intent}]
            result = timeline.insert(time1)


    elif len(recepient_list) > 0:
        machine_action = "select"
        time1 = [{"session_id": session_id, "time_stamp": time_stamp, "user_id": user_id, "message": utterances,
                  "machine_action": "select", "intent": intent}]
        result = timeline.insert(time1)


    elif result_timeline['machine_action']=="confirm":
            if len(recepient_list)==1:
                machine_action="execute"
                time1 = [
                    {"session_id": session_id, "time_stamp": time_stamp, "user_id": user_id, "message": utterances,
                     "machine_action": "execute","intent":intent}]
                result = timeline.insert(time1)

            elif len(recepient_list)==0:
                machine_action = "others"
                time1 = [
                    {"session_id": session_id, "time_stamp": time_stamp, "user_id": user_id, "message": utterances,
                     "machine_action": "others","intent":intent}]
                result = timeline.insert(time1)

    elif recepient==None:
        machine_action="request"
        time1 = [{"session_id": session_id, "time_stamp": time_stamp, "user_id": user_id, "message": utterances,
                  "machine_action": "request","intent":intent}]
        result = timeline.insert(time1)

    elif len(recepient_list)>0:
        machine_action = "select"
        time1 = [{"session_id": session_id, "time_stamp": time_stamp, "user_id": user_id, "message": utterances,
                  "machine_action": "select","intent":intent}]
        result = timeline.insert(time1)


    if intent=="MAKE_CALL":
        final_data=generate_response.generate_MAKE_CALL_responses(recepient,recepient_list,users_data,machine_action)
    else:
        final_data = generate_response.generate_SEND_MESSAGE_responses(recepient, recepient_list, users_data,
                                                                    machine_action)
    print(final_data)
    user_requests.insert(final_data)
    response["success"] = True
    # return the data dictionary as a JSON response
    response["data"]=final_data
    return flask.jsonify(response)




if __name__ == "__main__":
    print(("* Loading Keras model and Flask starting server..."
           "please wait until server has fully started"))
    json_file = open('model.json', 'r')
    loaded_model_json = json_file.read()
    json_file.close()
    loaded_model = model_from_json(loaded_model_json)
    print("Loaded model from disk")
    loaded_model.load_weights("model.h5")

    #app.run(debug=True)
    app.run(host="0.0.0.0", port=2525)