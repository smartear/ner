AA, bck, low, unr, vwl
AE, fnt, low, unr, vwl
AH, cnt, mid, unr, vwl
AO, bck, lmd, rnd, vwl
AW, bck, cnt, low, rnd, smh, unr, vwl
AY, cnt, fnt, low, smh, unr, vwl
B, blb, stp, vcd
CH, alv, frc, stp, vls
D, alv, stp, vcd
DH, dnt, frc, vcd
EH, fnt, lmd, unr, vwl
ER, cnt, rzd, umd, vwl
EY, fnt, lmd, smh, unr, vwl
F, frc, lbd, vls
G, stp, vcd, vel
HH, apr, glt
IH, fnt, smh, unr, vwl
IY, fnt, hgh, unr, vwl
JH, alv, frc, stp, vcd
K, stp, vel, vls
L, alv, lat
M, blb, nas
N, alv, nas
NG, nas, vel
OW, bck, rnd, smh, umd, vwl
OY, bck, fnt, lmd, rnd, smh, unr, vwl
P, blb, stp, vls
R, alv, apr
S, alv, frc, vls
SH, frc, pla, vls
T, alv, stp, vls
TH, dnt, frc, vls
UH, bck, rnd, smh, vwl
UW, bck, hgh, rnd, vwl
V, frc, lbd, vcd
W, apr, lbv
Y, apr, pal
Z, alv, frc, vcd
ZH, frc, pla, vcd
