import numpy as np
from sklearn.metrics.pairwise import cosine_similarity as cos_sim

np.random.seed(1234)
from pymongo import MongoClient
client = MongoClient()
client = MongoClient('localhost', 27017)
db = client['user_requests']
contact_groups = db.contact_groups





def load_contacts(sender_name):
    users = db.contact_groups.find({"list_users": {"$in": [sender_name]}})
    contact_list = []

    for values in users:


        for v in values['list_users']:
            if v != sender_name and v not in contact_list:
                contact_list.append(v)


    users=db.contacts_users.find({"username": {"$in": contact_list}})

    full_name_phoneme=[]
    contact_list=[]
    first_name_phoneme=[]
    last_name_phoneme=[]
    nick_name_phoneme=[]

    for values in users:

        contact_list.append(values['username'])
        if len(values['names_phonemes'])>3:
            first_name_phoneme.append(values['names_phonemes'][0])
            last_name_phoneme.append(values['names_phonemes'][1])
            full_name_phoneme.append(values['names_phonemes'][2])
            nick_name_phoneme.append(values['names_phonemes'][3])
        
    return first_name_phoneme,last_name_phoneme,full_name_phoneme,nick_name_phoneme,contact_list








def compare_phonemes(P1, P2):
    score = 0
    for p1, p2 in zip(P1, P2):
        score += cos_sim(y.index(p1), y.index(p2))

    return score


def find_best_sub_match(phoneme1, phoneme2):
    l = len(phoneme1)
    L = len(phoneme2)
    highest_score = 0
    for i in range(l + L - 1):
        P1 = phoneme1[max(0, l - 1 - i):l + L - i - 1]
        P2 = phoneme2[max(0, i - l + 1):i + 1]
        highest_score = max(highest_score, compare_phonemes(P1, P2))
    return highest_score





def match_contacts(user_says,user_says_phoneme,sender):
    y = []
    with open('phone_features.txt', 'r') as f:
        for line in f:
            line = line.strip('\n').split(',')
            y.append(line[0])

    first_name_phoneme, last_name_phoneme, full_name_phoneme, nick_name_phoneme, contact_list = load_contacts(
        sender)

    for matching_name in [full_name_phoneme, first_name_phoneme, last_name_phoneme,nick_name_phoneme]:
        for i in range(len(matching_name)):

            u_score = []
            c_score = []
            foo = []
            # print(matching_name[i],user_says_phoneme)
            match_score = find_best_sub_match(user_says_phoneme, matching_name[i])

            match_score_norm_speech = np.round(match_score / len(user_says), 4)
            match_score_norm_contact = np.round(match_score / len(matching_name[i]), 4)

            # content score, user score
            u_score.append(match_score_norm_speech)
            c_score.append(match_score_norm_contact)

        top_matches_by_u_score = list(zip(contact_list,
                                          u_score))
        top_matches_by_c_score = list(zip(contact_list,
                                          c_score))

        top_matches_by_u_score.sort(key=lambda x: -x[1])
        top_matches_by_c_score.sort(key=lambda x: -x[1])

        # print("matching speech rate")
        foo1 = [x for x in top_matches_by_u_score[:10] if x[1] >= 0.5]
        for x in top_matches_by_u_score[:10]:
            if x[1] >= 0.7:
                foo1.append(x)

        # print("matching contact name rate")
        foo2 = [x for x in top_matches_by_c_score[:10] if x[1] >= 0.5]

        if foo1 and foo2:
            for x in foo1:
                if x[0] in list(zip(*foo2))[0]:
                    i = list(zip(*foo2))[0].index(x[0])
                    foo.append((x[0], x[1], foo2[i][1]))
                    print("foo", foo)
        foo.sort(key=lambda x: -x[2])
        foo.sort(key=lambda x: -x[1])
        i = i + 1
        result=foo
        print(result)
        persons = []
        if len(result) == 1:
            persons.append(result[0][0])
        else:
            for i in range(1, len(result)):

                if result[i - 1][1] - result[i][1] < 0.1 or result[i - 1][2] - result[i][2] < 0.1:
                    persons.append(result[i - 1])
                i = i + 1

        return persons


#print(match_contacts('yunshi zhao',['Y', 'AH', 'N', 'SH', 'IY', 'ZH', 'AW']))