# -*- coding: utf-8 -*-
from g2p_en import g2p


def grapheme_to_phoneme(test_list):
    result=[]
    for values in test_list:
        inter = g2p(values)
        inter=give_result(inter)
        result.append(inter)
    return result

def give_result(text):
    result = []
    final_result=[]
    for values in text:
        S = ''
        for d in values:

            if d.isdigit() == False and d !=' ':
                S = S + d

        result.append(S)
    for values in result:
        if values is not '':
            final_result.append(str(values))
    return final_result

